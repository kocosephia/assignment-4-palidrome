﻿namespace Palindrome
{
    partial class Palindrome
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.openFileBox = new System.Windows.Forms.OpenFileDialog();
            this.loadFileBtn = new System.Windows.Forms.Button();
            this.textBox = new System.Windows.Forms.TextBox();
            this.exitBtn = new System.Windows.Forms.Button();
            this.introText = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // openFileBox
            // 
            this.openFileBox.FileName = "openFileBox";
            // 
            // loadFileBtn
            // 
            this.loadFileBtn.Location = new System.Drawing.Point(12, 39);
            this.loadFileBtn.Name = "loadFileBtn";
            this.loadFileBtn.Size = new System.Drawing.Size(75, 23);
            this.loadFileBtn.TabIndex = 1;
            this.loadFileBtn.Text = "Load File";
            this.loadFileBtn.UseVisualStyleBackColor = true;
            this.loadFileBtn.Click += new System.EventHandler(this.loadFileBtn_Click);
            // 
            // textBox
            // 
            this.textBox.Location = new System.Drawing.Point(32, 85);
            this.textBox.Multiline = true;
            this.textBox.Name = "textBox";
            this.textBox.Size = new System.Drawing.Size(628, 106);
            this.textBox.TabIndex = 2;
            // 
            // exitBtn
            // 
            this.exitBtn.Location = new System.Drawing.Point(12, 228);
            this.exitBtn.Name = "exitBtn";
            this.exitBtn.Size = new System.Drawing.Size(75, 23);
            this.exitBtn.TabIndex = 3;
            this.exitBtn.Text = "Quit";
            this.exitBtn.UseVisualStyleBackColor = true;
            this.exitBtn.Click += new System.EventHandler(this.exitBtn_Click);
            // 
            // introText
            // 
            this.introText.AutoSize = true;
            this.introText.Location = new System.Drawing.Point(12, 9);
            this.introText.Name = "introText";
            this.introText.Size = new System.Drawing.Size(675, 13);
            this.introText.TabIndex = 4;
            this.introText.Text = "Welcome to the Palindrome Program. Load a text file and the program will locate t" +
    "he longest palindrome in that file. (Cannot cancel dialog box.)";
            // 
            // Palindrome
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(698, 272);
            this.Controls.Add(this.introText);
            this.Controls.Add(this.exitBtn);
            this.Controls.Add(this.textBox);
            this.Controls.Add(this.loadFileBtn);
            this.Name = "Palindrome";
            this.Text = "Palindrome - Assignment 4 -Micah West";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog openFileBox;
        private System.Windows.Forms.Button loadFileBtn;
        private System.Windows.Forms.TextBox textBox;
        private System.Windows.Forms.Button exitBtn;
        private System.Windows.Forms.Label introText;
    }
}

