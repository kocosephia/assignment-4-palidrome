﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Palindrome
{
    public partial class Palindrome : Form
    {
        public Palindrome()
        {
            InitializeComponent();
        }

        private void loadFileBtn_Click(object sender, EventArgs e)
        {
            OpenFileDialog fileName = new OpenFileDialog();
            fileName.Filter = "Text Files (.txt)|*.txt|All Files (*.*)|*.*";
            fileName.FilterIndex = 1;
            string theFileName = "failed";
            fileName.ShowDialog();
            theFileName = fileName.FileName;

            if (!fileName.CheckFileExists)
                Application.Exit();

            
           string[] totalText = System.IO.File.ReadAllLines(theFileName);
            char[] newLine = new char[10000];
            int counter = 0;
           foreach (string line in totalText){
               if (line[line.Length - 1] == '\n')
               {
                   for (int i = 0; i < line[line.Length]; i++)
                   {
                       newLine[counter] = line[i];
                       counter++;
                   }
               }
               else
               {
                   
                 for (int i = 0; i < line.Length; i++)
                 {
                     newLine[counter] = line[i];
                     counter++;
                 }

               }
           }
           char[] shortened = new char[counter];
           Array.Copy(newLine, 0, shortened, 0, counter);
           string longestOne = FindPalindrome(shortened);
           textBox.Text = longestOne;


        }

        public string FindPalindrome(char[] inputText)
        {
            char[] longest = new char[1000];
            int length = 0;
            int midLetterLocation = 0;
            for(int g = 1; g < inputText.Length; g++){
                for (int i = 0; i < inputText.Length; i++)
                {
                    if (g+i < inputText.Length && g-i >= 0 && (inputText[g - i] == inputText[g + i]))
                    {
                        if (i > length)
                        {
                            length = i;
                            midLetterLocation = g;
                        }
                    }
                    else
                        break;
                    }
                }
            string complete;
            if(midLetterLocation == 0)
                complete = "No palindromes found";
            else{
                char[] longestPal = new char[length * 2 + 1];
                Array.Copy(inputText, midLetterLocation - length, longestPal, 0, length * 2+1);
                complete = new string(longestPal);
            }

            return complete;
        }

        private void exitBtn_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

    }
}
